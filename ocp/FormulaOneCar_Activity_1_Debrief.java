public class FormulaOneCar_Activity_1_Debrief {

    /*
     * Requirement:
     * Engine is a class developed by another team. Since every car needs an engine, we want to reference it here.
     * So the requirement needs you to add a private "Engine" property and isEngineWorking() method in this class.
     * 
     * Do you need to extend the class again?
     */

    private String carType;
    protected Sensors[] sensors;

    public boolean areSensorsWorking() throws NotWorkingException {
        // .. code
    }

    public boolean isEngineWorking() throws EngineNotWorkingException {
        // .. code
    }

    public boolean isEverythingWorking() {
        boolean allOk = false;

        try {
            allOk = areSensorsWorking() && isEngineWorking();
        } catch (Exception e) {
            // log exception
        }

        return isOk;
    }
    
}
