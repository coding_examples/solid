public class FormulaOneCarExample1 {

    /*
     * New requirement:
     * Instead of just returning a boolean, the team wants to start throwing errors when
     * critical functions such as sensors fail. Also, assume this class is heavily used.
     * 
     * How would you do this? Can you change the code directly? 
     */

    protected Sensors[] sensors;

    public boolean isEverythingWorking() {
        boolean isOk;
        for(Sensor sensor : sensors) {
            isOk = isOk && sensor.check();
        }

        return isOk;
    }
}