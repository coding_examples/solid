/*
 * Requirement:
 * DragRaceCar has steer(), brake(), accelerate() and activateNitro().
 * FormulaOneCar has steer(), brake(), accelerate() and isSensorsWorking().
 * VintageCar has steer(), brake() and accelerate().
 * 
 * How will your code change to implement this requirement?
 */

public interface BasicCar {
    void steer(double angle);
    void brake();
    void accelerate(double force);
}

public interface FormulaOneCarFunctions extends BasicCar {
    boolean isSensorsWorking(String[] sensorArray);
}

public interface DragRaceCarFunctions extends BasicCar {
    void activateNitro();
}

public class FormulaOneCar implements FormulaOneCarFunctions {}
public class DragRaceCar implements DragRaceCarFunctions {}
public class VintageCar implements BasicCar {}