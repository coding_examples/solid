/*
 * Requirement:
 * DragRaceCar has steer(), brake(), accelerate() and activateNitro().
 * FormulaOneCar has steer(), brake(), accelerate() and isSensorsWorking().
 * VintageCar has steer(), brake() and accelerate().
 * 
 * How will your code change to implement this requirement?
 */

public interface BasicCar {
    void steer(double angle);
    void brake();
    void accelerate(double force);
    boolean isSensorsWorking(String[] sensorArray);
    void activateNitro();
}

public class FormulaOneCar {}
public class DragRaceCar {}
public class VintageCar {}