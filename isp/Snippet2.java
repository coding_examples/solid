/*
 * Context:
 * You have just been given the requirement to DragRaceCar with nitro boost.
 * But the problem is:
 * - The implementation of DragRaceCar does not have any sensors at all.
 * - FormulaOneCar is already widely used.
 * - BasicCar is already being used by other car implementations such as IndyCar, RallyCar and so on.
 * 
 * How would you solve this? 
 */

public interface BasicCar {
    void steer(double angle);
    void brake();
    void accelerate(double force);
    boolean isSensorsWorking(String[] sensorArray);
}

public class FormulaOneCar implements BasicCar {
    // .. code
}