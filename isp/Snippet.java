public interface BasicCar {
    void steer(double angle);
    void brake();
    void accelerate(double force);
    boolean isSensorsWorking(String[] sensorArray);
    void nitroBoost();
    void deployParachute();
}
