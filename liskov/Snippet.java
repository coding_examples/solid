class ClassA {
    public void methodA() {
        // .. code
    }
}

class classB extends ClassA {

    @Override
    public void methodA() {
        // Method has been overridden and reimplemented.
        // .. code

        // .. upon some logic
        if (condition)
            throw new Exception();
    }
}

// Form 1
ClassB b = new ClassB();
b.methodA();

// Form 2
ClassB b = new ClassA();
b.methodA();