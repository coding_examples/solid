public class RaceTruck_Activity_2_Debrief {
    
    private ArrayList<Sensor> sensors;

    public boolean isSensorsWorking() {
        boolean isWorking;

        for (Sensor sensor : sensors) {
            isWorking = isWorking && sensor.check();
        }

        return isWorking;
    }

    // .. rest of the class
}
