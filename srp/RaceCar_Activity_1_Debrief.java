public class RaceCar_Activity1_Debrief {
    
    private ArrayList<Sensor> sensors;

    public boolean isSensorsWorking() {
        boolean isWorking = true;

        for (Sensor sensor : sensors) {
            isWorking = isWorking && sensor.check();
        }

        return isWorking;
    }

    // .. rest of the class
}

// Create new file Sensor.java
public interface Sensor {
    public boolean check();
}

// Create new GyroSensor.java
public class GyroSensor implements Sensor {
    public boolean check() {
        // ...
        // code to check if sensors are working
        // ...
        return false;
    }
}

// Create new file for FuelSensor.java
public FuelSensor implements Sensor {
    public boolean check() {
        // ..
    }
}