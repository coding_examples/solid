public class RaceCarExample1 {
    private String model;
    private boolean fuelType;
    private double fundsSpent;
}

/*
 * Reasoning 1:
 * - Every racing car has some cost attached to it.
 * - This expense can come from various sources: parts of the car, maintenance, engineering costs and others.
 * - Every car potentially can have different costs.
 * - The team management is the one who assigns these funds.
 * - Thus, the easiest way to keep track of how much money was spent on the car is to make it have a property.
 * 
 * Reasoning 2:
 * - All of the above is true, except the last one.
 * - The car entity does not have knowledge on how much funds was spent on it. It is the "Management" entity that does.
 * - Management maintains a mapping of funds spent for every car externally, the car entity does not need to know or care about this.
 */
