/*
 * Here, the Mechanic class' fix() method is aware of the implementation of the Car class.
 */

public class Mechanic {
    public void fix(Car c) {
        if (c instanceof FormulaOneCar) {
            c.getTyrePressure();
            c.getGyroSensorData();
            c.getVEngineData();
            // code...
        }
        if (c instanceof IndyCar) {
            c.getTyrePressure();
            c.getGForceSensorData();
            c.getInlineEngineData();
            // .. code
        }
    }
}
