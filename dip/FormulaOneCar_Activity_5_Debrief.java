/*
 * The team has built FormulaOneEngine but is also working on two new engines:
 * - V2Engine
 * - HybridEngine
 * 
 * What changes would you make such that the formula one car is not aware of the type of engine,
 * and only interacts with it through a contract?
 */

public class FormulaOneCar {
    private String model;
    private Engine engine;

    public FormulaOneCar(Engine engine) {
        this.engine = engine;
    }

    public void checkEngine() {
        boolean flag = engine.check();
        Data[] parameters = engine.getEngineParameters();
        // .. code
    }
}

Engine engine = new V2Engine();
FormulaOneCar car = new FormulaOneCar(engine);
car.checkEngine();

public interface Engine {
    // .. methods
    boolean check();
    Data[] getEngineParameters();
}