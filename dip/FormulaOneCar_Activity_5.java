/*
 * The team has built FormulaOneEngine but is also working on two new engines:
 * - V2Engine
 * - HybridEngine
 * 
 * What changes would you make such that the formula one car is not aware of the type of engine,
 * and only interacts with it through a contract?
 */

public class FormulaOneCar {
    private String model;
    private FormulaOneEngine formulaOneEngine;

    public FormulaOneCar() {}

    public void checkEngine() {
        boolean flag = formulaOneEngine.check();
        Data[] parameters = formulaOneEngine.getEngineParameters();
        // .. code
    }
    
}
